import numpy as np
from scipy import integrate
import scipy.sparse as sparse
import scipy.interpolate
import pdb

from cs import CanonicalSystem
from exponential_integration import exp_eul_step
import derivative_matrices as der_mtrx

class DMPs_cartesian():
    """
    Implementation of discrete dxnamic Movement Primitives in cartesian space,as described in
    [1] Park, D. H., Hoffmann, H., Pastor, P., & Schaal, S. (2008, December).
        Movement reproduction and obstacle avoidance with dxnamic movement primitives and potential fields.
        In Humanoid Robots, 2008. Humanoids 2008. 8th IEEE-RAS International Conference on (pp. 91-98). IEEE.
    [2] Hoffmann, H., Pastor, P., Park, D. H., & Schaal, S. (2009, May).
        Biologically-inspired dxnamical systems for movement generation: automatic real-time goal adaptation and obstacle avoidance.
        In Robotics and Automation, 2009. ICRA'09. IEEE International Conference on (pp. 2587-2592). IEEE.
    """

    def __init__(self, n_dmps = 3, n_bfs = 50, dt = 0.01, x0 = None, goal = None, T = 1.0, K = None, D = None, w = None, tol = 0.1, alpha_s = 4.0, **kwargs):
        """
        n_dmps int   : number of dynamic movement primitives (i.e. dimensions)
        n_bfs int    : number of basis functions per DMP (actually, they will be one more)
        dt float     : timestep for simulation
        x0 array     : initial state of DMPs
        goal array   : goal state of DMPs
        T float      : final time
        K float      : elastic parameter in the dynamical system
        D float      : damping parameter in the dynamical system
        w array      : associated weights
        tol float    : tolerance
        alpha_s float: constant of the Canonical System
        """

        # Tolerance for the accuracy of the movement: the trajectory will stop when || x - g || <= tol
        self.tol = tol
        self.n_dmps = n_dmps
        self.n_bfs = n_bfs

        # Default values give as in [2]
        if K is None:
            K = np.ones(n_dmps) * 1050.0
        self.K = K
        if D is None:
            D = 2 * np.sqrt(self.K)
        self.D = D

        # Create the matrix of the linear component of the problem
        self.linear_part = np.zeros([2 * self.n_dmps, 2 * self.n_dmps])
        for d in range(n_dmps):
            self.linear_part[2 * d, 2 * d] = - self.D[d]
            self.linear_part[2 * d, 2 * d + 1] = - self.K[d]
            self.linear_part[2 * d + 1, 2 * d] = 1.

        # Set up the CS
        self.cs = CanonicalSystem(dt = dt, run_time = T, alpha_s = alpha_s)

        # Set up the DMP system
        if x0 is None:
            x0 = np.zeros(self.n_dmps)
        if goal is None:
            goal = np.zeros(self.n_dmps)
        self.x0 = x0
        self.goal = goal
        self.reset_state()
        self.gen_centers()
        self.gen_width()

        # If no weights are give, they are set to zero
        if w is None:
            w = np.zeros([self.n_dmps, self.n_bfs + 1])
        self.w = w

    def gen_centers(self):
        """
        Set the centres of the basis functions to be spaced evenly throughout run time
        """
        self.c = np.exp(- self.cs.alpha_s * self.cs.run_time * ((np.cumsum(np.ones([1, self.n_bfs + 1])) - 1) / self.n_bfs))

    def gen_psi(self, s):
        """
        Generates the activity of the basis functions for a given canonical system rollout.
         s : array containing the rollout of the canonical system
        """
        c = np.reshape(self.c, [self.n_bfs + 1, 1])
        w = np.reshape(self.width, [self.n_bfs + 1,1 ])
        xi = w * np.power((s - c), 2.0)
        psi_set = np.exp(- xi)
        psi_set = np.nan_to_num(psi_set)
        return psi_set

    def gen_width(self):
        """
        Set the "widths" for the basis functions.
        """
        self.width = np.diff(self.c) ** (- 2.0)
        self.width = np.append(self.width, self.width[-1])

    def gen_weights(self, f_target):
        """
        Generate a set of weights over the basis functions such that the target forcing term trajectory is matched.
          f_target shaped n_dim x n_time_steps
        """

        # Generate the basis functions
        s_track = self.cs.rollout()
        psi_track = self.gen_psi(s_track)

        # Compute useful quantities
        sum_psi = np.sum(psi_track, 0)
        sum_psi_2 = sum_psi ** 2.0
        s_track_2 = s_track ** 2.0

        # Set up the minimization problem
        A = np.zeros([self.n_bfs + 1, self.n_bfs + 1])
        b = np.zeros([self.n_bfs + 1])

        # The matrix does not depend on f
        for k in range(self.n_bfs + 1):
            for h in range(k, self.n_bfs + 1):
                A[k, h] = integrate.simps(psi_track[k] * psi_track[h] * s_track_2 / sum_psi_2, s_track)
                A[h, k] = A[k, h].copy()

        # The problem is decoupled for each dimension
        for d in range(self.n_dmps):

            # Create the matrix of the regression problem
            for k in range(self.n_bfs + 1):
                b[k] = integrate.simps(f_target[d] * psi_track[k] * s_track / sum_psi, s_track)

            # Solve the minimization problem
            self.w[d] = np.linalg.solve(A, b)
        self.w = np.nan_to_num(self.w)

    def imitate_path(self, x_des, dx_des = None, ddx_des = None, t_des = None, **kwargs):
        """
        Takes in a desired trajectory and generates the set of system parameters that best realize this path.
          x_des array shaped num_timesteps x n_dmps
          t_des 1D array of num_timesteps component
          g_w boolean, used to separate the one-shot learning from the regression over multiple demonstrations
        """

        ## Set initial state and goal
        self.x0 = x_des[0].copy()
        self.goal = x_des[-1].copy()

        ## Interpolate the desired trajectory on the discretized time domain defined by the parameters of DMPs

        # Initialize
        path = np.zeros([self.cs.timesteps, self.n_dmps])

        # Generate function to interpolate the desired trajectory
        if t_des is None:
            t_des = np.linspace(0, self.cs.run_time, x_des.shape[0])
        else:
            t_des -= t_des[0]
            t_des /= t_des[-1]
            t_des *= self.cs.run_time
        time = np.linspace(0., self.cs.run_time, self.cs.timesteps)
        for d in range(self.n_dmps):
            # Piecewise linear interpolation
            path_gen = scipy.interpolate.interp1d(t_des, x_des[:, d]) # this is a function
            path[:, d] = path_gen(time)
        x_des = path

        # Second order estimates of the derivatives (the last non centered, all the others centered)
        if dx_des is None:
            D1 = der_mtrx.compute_D1(self.cs.timesteps, self.cs.dt)
            dx_des = np.dot(D1, x_des)
        else:
            dpath = np.zeros([self.cs.timesteps, self.n_dmps])
            dpath_gen = scipy.interpolate.interp1d(t_des, dx_des[:, d]) # this is a function
            dpath[:, d] = dpath_gen(time)
            dx_des = dpath
        if ddx_des is None:
            D2 = der_mtrx.compute_D2(self.cs.timesteps, self.cs.dt)
            ddx_des = np.dot(D2, x_des)
        else:
            ddpath = np.zeros([self.cs.timesteps, self.n_dmps])
            ddpath_gen = scipy.interpolate.interp1d(t_des, ddx_des[:, d]) # this is a function
            ddpath[:, d] = ddpath_gen(time)
            ddx_des = ddpath

        # Find the force required to move along this trajectory
        f_target = np.zeros([self.n_dmps, self.cs.timesteps])
        s_track = self.cs.rollout()
        for d in range(self.n_dmps):
            f_target[d] = ddx_des[:, d] / self.K[d] - (self.goal[d] - x_des[:, d]) + self.D[d] / self.K[d] * dx_des[:, d] + (self.goal[d] - self.x0[d]) * s_track

        # Efficiently generate weights to realize f_target
        self.gen_weights(f_target)
        self.reset_state()
        return f_target

    def reset_state(self, v0 = None, **kwargs):
        """
        Reset the system state
        """
        self.x = self.x0.copy()
        if v0 is None:
            v0 = 0.0 * self.x0
        self.dx = v0
        self.ddx = np.zeros(self.n_dmps)
        self.cs.reset_state()

    def rollout(self, tau = 1.0, v0 = None, **kwargs):
        """
        Generate a system trial, no feedback is incorporated.
          tau scalar, time rescaling constant
          v0 scalar, initial velocity of the system
        """
        # Reset the state of the DMP
        if v0 is None:
            v0 = 0.0 * self.x0
        self.reset_state(v0 = v0)
        # Set up tracking vectors
        x_track = np.zeros((0, self.n_dmps))
        dx_track = np.zeros((0, self.n_dmps))
        ddx_track = np.zeros((0, self.n_dmps))
        # Add the initial value to the tracking vectors
        x_track = np.append(x_track, [self.x0], axis = 0)
        dx_track = np.append(dx_track, [v0], axis = 0)
        ddx_track = np.append(ddx_track, [0.0 * self.x0], axis = 0)
        flag = False # flag to decide if the execution has to be stopped
        t = 0
        t_track = [0]
        while (not flag):
            # Run and record timestep
            x_track_s, dx_track_s, ddx_track_s = self.step(tau = tau)
            x_track = np.append(x_track, [x_track_s], axis=0)
            dx_track = np.append(dx_track, [dx_track_s],axis=0)
            ddx_track = np.append(ddx_track, [ddx_track_s],axis=0)
            t_track.append(t_track[-1] + self.cs.dt)
            t += 1
            err_abs = np.linalg.norm(x_track_s - self.goal)
            err_rel = err_abs / (np.linalg.norm(self.goal - self.x0) + 1e-14)
            flag = err_rel <= self.tol
        return x_track, dx_track, ddx_track, t_track

    def step(self, tau = 1.0, **kwargs):
        """
        Run the DMP system for a single timestep.
          tau float: time rescaling constant
        """

        # Run canonical system
        s = self.cs.step(tau = tau)

        # Generate basis function activation
        psi = self.gen_psi(s)
        f = np.zeros(self.n_dmps)

        # Initialize the integration scheme
        state = np.zeros(2 * self.n_dmps)
        affine_part = np.zeros(2 * self.n_dmps)
        for d in range(self.n_dmps):
            f[d] = (np.dot(psi[:, 0], self.w[d, :])) / (np.sum(psi[:, 0])) * s
            f[d] = np.nan_to_num(f[d])

        # Set the state vector and the affine part of the scheme
        state[range(0, 2 * self.n_dmps, 2)] = self.dx
        state[range(1, 2 * self.n_dmps + 1, 2)] = self.x
        affine_part[range(0, 2 * self.n_dmps, 2)] = self.K * (self.goal * (1. - s) + self.x0 * s + f)
        affine_part[range(1, 2 * self.n_dmps + 1, 2)] = 0.0

        # Perform the integration step
        state = exp_eul_step(state, self.linear_part, affine_part, self.cs.dt)

        # Extract position and velocity
        self.x = state[range(1, 2 * self.n_dmps + 1, 2)]
        self.dx = state[range(0, 2 * self.n_dmps, 2)]

        # Compute the acceleration
        self.ddx = self.K / (tau ** 2.0) * ((self.goal - self.x) - (self.goal - self.x0) * s + f) - self.D / tau * self.dx
        return self.x, self.dx, self.ddx