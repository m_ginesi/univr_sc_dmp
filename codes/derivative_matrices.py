import numpy as np

def compute_D1(n, dt):
    """
    Compute the matrices used to estimate the first derivative.
      n float  : dimensionality
      dt float : timestep
    """
    d1_p = np.ones([n - 1])
    d1_p[0] = 4.
    d1_m = - np.ones([n-1])
    d1_m[-1] = - 4.
    D1 = np.diag(d1_p, 1) + np.diag(d1_m, -1)
    D1[0,0] = - 3.
    D1[0, 2] = -1.
    D1[-1, -3] = 1.
    D1[-1,-1] = 3.
    D1 /= 2 * dt
    return D1

def compute_D2(n, dt):
    """
    Compute the matrices used to estimate the first derivative.
      n float  : dimensionality
      dt float : timestep
    """
    d2_p = np.ones([n-1])
    d2_p[0] = -5.
    d2_m = np.ones([n-1])
    d2_m[-1] = -5.
    d2_c = - 2. * np.ones([n])
    d2_c[0] = 2.
    d2_c[-1] = 2.
    D2 = np.diag(d2_p, 1) + np.diag(d2_c, 0) + np.diag(d2_m, -1)
    D2[0, 2] = 4.
    D2[0, 3] = -1.
    D2[-1, -3] = 4.
    D2[-1, -4] = -1.
    D2 /= dt ** 2
    return D2