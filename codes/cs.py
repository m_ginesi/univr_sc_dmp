import numpy as np

class CanonicalSystem():
    """
    Implementation of the canonical dynamical system
    """

    def __init__(self, dt, alpha_s = 4.0, run_time = 1.0, **kwargs):

        # Assign the parameters
        self.alpha_s = alpha_s
        self.run_time = run_time
        self.dt = dt

        self.timesteps = int(self.run_time / self.dt) + 1
        self.reset_state()

    def reset_state(self):
        """
        Reset the system state
        """
        self.s = 1.0

    def rollout(self, tau = 1.0, **kwargs):
        """
        Generate s.
        """
        timesteps = int(self.timesteps / tau)
        s_track = np.zeros(timesteps)
        self.reset_state()
        for t in range(timesteps):
            s_track[t] = self.s
            self.step(tau = tau)
        return s_track

    def step(self, tau = 1.0,**kwargs):
        """
        Generate a single step of s.
          tau float
        """
        # Since the canonical system is linear, we use an exponential method (which is exact)
        const = - self.alpha_s / tau
        self.s *= np.exp(const * self.dt)
        return self.s