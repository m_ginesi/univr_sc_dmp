# README #

This repository contains the material used for the DMP seminar in University of Verona.

## CONTENTS ##

The contents of this repository are:
  * _notes.pdf_ : notes of the course;
  * _codes/_ : folder containing the codes used to implement DMPs in Python 3;
  * _demos/_ : folder containing the codes used to test the implementation

## CONTACT ##

You can contact me by mail at

michele.ginesi@univr.it