"""
At first, we import the packages needed to implement our demo
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
import sys
sys.path.insert(0, '../codes/')
sys.path.insert(0, 'codes/')

from dmp_cartesian import DMPs_cartesian as dmp

"""
We now create the trajectory to learn
"""

t_f = 1.0 * np.pi # final time
t_steps = 10 ** 3 # number time steps
t = np.linspace(0.0, t_f, t_steps) # time domain

a_x = 1.0 / np.pi
b_x = 1.0
a_y = 1.0 / np.pi
b_y = 1.0

x = a_x * t * np.cos(b_x*t)
y = a_y * t * np.sin(b_y*t)

x_des = np.ndarray([t_steps, 2])
x_des[:, 0] = x
x_des[:, 1] = y

x_des -= x_des[0]

"""
Initialization of the DMP object and learning phase
"""

# Parameters
n_dim = 2   # number of dimensions
K = 1000.0 * np.ones(2) # elastic constant (one value for each direction)
n_bfs = 30  # number of basis functions (per direction) used for the approximation of the forcing term
dt = 0.01   # numerical integration step
alpha = 4.0 # canonical system factor
tol = 2e-02 # tolerance for considering the goal reached

# Initialization
dmp_1 = dmp(n_dmps = n_dim, n_bfs = n_bfs, K = K, dt = dt, alpha_s = alpha, tol = tol)

# Learning phase
dmp_1.imitate_path(x_des = x_des)

"""
Execution with no changes
"""

x_classical, _, _, _ = dmp_1.rollout()

"""
Execution with moving goal position
"""

dmp_1.reset_state() # Reset the DMP system

# Initialization of the array containing the evolution of the trajectory
x_moving = np.zeros([1, dmp_1.n_dmps])

flag = False    # True when we can stop the execution of the trajectory
count = 0   # Counter to decide when to move the goal

while (not flag):
    count += 1
    # Integration step
    x_track_s, _, _ = dmp_1.step()

    # Add the new value to the recording array
    x_moving = np.append(x_moving, [x_track_s], axis = 0)

    # Check if we are close to the goal
    flag = (np.linalg.norm(x_track_s - dmp_1.goal) / np.linalg.norm(dmp_1.goal - dmp_1.x0) <= dmp_1.tol)

    # Move the goal
    if (count > dmp_1.cs.timesteps / 4) and (count < dmp_1.cs.timesteps / 2):
        dmp_1.goal += np.array([-1.0, -1.0]) / 2.0 * dmp_1.cs.dt

"""
Execution with moved goal position
"""

dmp_1.reset_state()
x_moved, _, _, _ = dmp_1.rollout()

"""
Plot
"""

plt.figure()
plt.plot(x_classical[:,0], x_classical[:,1], 'r', label = 'learned')   # classical execution
plt.plot(x_des[:,0], x_des[:, 1], ':g', label = 'desired') # desired trajectory
plt.plot(x_moving[:,0], x_moving[:,1], 'b', label = 'moving goal')   # classical execution
plt.plot(x_moved[:,0], x_moved[:,1], 'm', label = 'moved goal')   # classical execution

# Plot of the initial and final positions
plt.plot(dmp_1.x0[0], dmp_1.x0[1], '.k')
plt.plot(dmp_1.goal[0], dmp_1.goal[1], '.k')
plt.plot(x_des[-1][0], x_des[-1][1], '.k')
plt.text(dmp_1.x0[0]-0.07, dmp_1.x0[1]-0.07, r"$\mathbf{x}_0 = \mathbf{x}_0' $", fontsize = 16)
plt.text(dmp_1.goal[0]+0.01, dmp_1.goal[1]-0.05, r"$\mathbf{g}'$", fontsize = 16)
plt.text(x_des[-1][0]+0.01, x_des[-1][1]-0.05, r"$\mathbf{g}$", fontsize = 16)

plt.axis('equal')
plt.legend(loc = 'best')

plt.show()